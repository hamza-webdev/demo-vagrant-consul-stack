#!/bin/bash

## install master consul

IP=$(hostname -I | awk '{print $2}')
echo "START - install app - "$IP

echo "[1]: install utils"
apt-get update -qq >/dev/null
apt-get install -qq -y wget unzip dnsutils python python-flask python-pip nginx >/dev/null

echo "[2]: install consul"
#wget -q https://releases.hashicorp.com/consul/1.4.0/consul_1.4.0_linux_amd64.zip
unzip /home/vagrant/consul.zip
mv /home/vagrant/consul /usr/local/bin/


echo "[3]: create user/group and directory"
groupadd --system consul
useradd -s /sbin/nologin --system -g consul consul
mkdir -p /var/lib/consul
chown -R consul:consul /var/lib/consul
chmod -R 775 /var/lib/consul
mkdir /etc/consul.d
chown -R consul:consul /etc/consul.d

echo "[4]: consul configuration for agent"
echo '{
    "advertise_addr": "'$IP'",
    "bind_addr": "'$IP'",
    "client_addr": "0.0.0.0",
    "datacenter": "mydc",
    "data_dir": "/var/lib/consul",
    "domain": "consul",
    "enable_script_checks": true,
    "dns_config": {
            "enable_truncate": true,
            "only_passing": true
        },
    "enable_syslog": true,
    "encrypt": "TeLbPpWX41zMM3vfLwHHfQ==",
    "leave_on_terminate": true,
    "log_level": "INFO",
    "rejoin_after_leave": true,
    "retry_join": [
    "192.168.57.10"
    ]
}' > /etc/consul.d/config.json

echo "[5]: consul create service systemd"
echo '[Unit]
Description=Consul Service Discovery Agent
Documentation=https://www.consul.io/
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=consul
Group=consul
ExecStart=/usr/local/bin/consul agent \
  -node='$IP' \
  -config-dir=/etc/consul.d

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/consul.service

echo "[6]: consul start service"
systemctl enable consul
service consul start


echo "[7]: install myapp"
pip install -q prometheus_client
mkdir /var/myapp/
echo "#!/usr/bin/python
from flask import Flask,request,Response
from prometheus_client import (generate_latest,CONTENT_TYPE_LATEST )
import socket
app = Flask(__name__)


@app.route('/')
def hello_world():
    hostname = socket.gethostname()
    message = 'Bonjour, je suis ' + hostname + '\n'
    return message

@app.route('/metrics')
def metrics():
    return Response(generate_latest(),mimetype=CONTENT_TYPE_LATEST)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
">/var/myapp/app.py
chmod 755 /var/myapp/app.py

echo "[8]: install myapp service"
echo '[Unit]
Description=aMyApp service
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=root
Group=root
ExecStart=/var/myapp/app.py \
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=myapp

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/myapp.service
systemctl enable myapp
service myapp start


echo "[9]: add user xavki / psswd= password"
useradd -m -s /bin/bash -p sa3tHJ3/KuYvI -U xavki
echo "%xavki ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/xavki


echo "[10]: install consul service"
echo '
{"service":
 {
  "name": "myapp", 
  "tags": ["myapp","python","metrics","url:myapp.localhost"], 
  "port": 8080,
  "check": {
    "http": "http://localhost:8080/",
    "interval": "3s"
  }
 }
}
' >/etc/consul.d/service_myapp.json
consul reload

echo "[11]: MESH - install linkerd"

apt-get -y -q install openjdk-11-jdk >/dev/null
wget -q https://github.com/linkerd/linkerd/releases/download/1.6.2.2/linkerd-1.6.2.2-exec -P /usr/local/bin
chmod 755 /usr/local/bin/linkerd-1.6.2.2-exec
mkdir /etc/linkerd/
echo '
admin:
  ip: 0.0.0.0
  port: 9990
routers:
- protocol: http
  label: /http-consul
  service:
    responseClassifier:
      kind: io.l5d.http.retryableIdempotent5XX
  servers:
  - port: 4141
    ip: 0.0.0.0
  identifier:
   kind: io.l5d.path
   segments: 1
   consume: true
  dtab: |
    /svc => /#/192.168.57.10/mydc;
  client:
    failureAccrual:
      kind: io.l5d.successRateWindowed
      successRate: 0.7
      window: 60
    loadBalancer:
      kind: p2c
      maxEffort: 3
- protocol: http
  label: /http-consul2
  servers:
  - port: 4040
    ip: 0.0.0.0
  originator: true
  identifier:
    kind: io.l5d.header.token
  service:
    totalTimeoutMs: 20000
    retries:
      budget:
        minRetriesPerSec: 5
        percentCanRetry: 0.2
        ttlSecs: 15
      backoff:
        kind: jittered
        minMs: 2000
        maxMs: 5000
  dtab: |
    /svc => /#/192.168.57.10/mydc;
  client:
    failureAccrual:
      kind: io.l5d.successRateWindowed
      successRate: 0.7
      window: 60
    loadBalancer:
      kind: p2c
      maxEffort: 3

namers:
- kind: io.l5d.consul
  host: 192.168.57.10
  includeTag: false
  useHealthCheck: true
  prefix: /192.168.57.10
  consistencyMode: stale
  failFast: true

telemetry:
- kind: io.l5d.prometheus
  path: /metrics
' > /etc/linkerd/linkerd.yml

echo '[Unit]
Description=Linkerd
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=root
Group=root
ExecStart=/usr/local/bin/linkerd-1.6.2.2-exec /etc/linkerd/linkerd.yml 
KillSignal=SIGINT
TimeoutStopSec=10
Restart=on-failure
SyslogIdentifier=linkerd

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/linkerd.service
systemctl enable linkerd
service linkerd start

echo "[12]: install consul-template"
unzip /home/vagrant/consul-template.zip
rm -f /home/vagrant/consul-template.zip
mv /home/vagrant/consul-template /usr/local/bin/


groupadd --system consul
useradd -s /sbin/nologin --system -g consul consul
chown consul:consul /usr/local/bin/consul-template
chmod 755 /usr/local/bin/consul-template
mkdir /etc/consul-template
chown consul:consul /etc/consul-template
chmod 775 /etc/consul-template

echo "[13]: create service systemd consul-template"
echo '
[Unit]
Description=Consul Template adon to consul
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=root
Group=consul
ExecStart=/usr/local/bin/consul-template \
    -config /etc/consul-template/config.hcl

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul

[Install]
WantedBy=multi-user.target
' >/etc/systemd/system/consul-template.service


echo "[14]: create conf consul-template"
echo '
consul {
    address = "192.168.57.10:8500"
    retry {
        enabled = true
        attempts = 12
        backoff = "250ms"
    }
}

template {
    source      = "/etc/consul-template/homepage.ctmpl"
    destination = "/var/www/html/index.html"
}

template {
    source      = "/etc/consul-template/default.ctmpl"
    destination = "/etc/nginx/sites-available/default"
    command     = "service nginx reload"
}

'> /etc/consul-template/config.hcl

echo "[15]: create template for /var/www/html/index.html"
echo '
{{ key "app/var/www/html/index.html@mydc" }}
'>/etc/consul-template/homepage.ctmpl


echo "[15]: create template for /var/www/html/index.html"
echo '
{{ key "app/etc/nginx/sites-available/default@mydc" }}
'>/etc/consul-template/default.ctmpl

echo "[16]: start consul-template"

systemctl enable consul-template
service consul-template start

echo "END - install app - "$IP
