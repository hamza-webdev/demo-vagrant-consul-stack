#!/bin/bash

## install master consul

IP=$(hostname -I | awk '{print $2}')
echo "START - install haproxy - "$IP

echo "[1]: install utils"
apt-get update -qq >/dev/null
apt-get install -qq -y wget unzip haproxy >/dev/null

echo "[2]: install consul-template"
unzip /home/vagrant/consul-template.zip
rm -f /home/vagrant/consul-template.zip
mv /home/vagrant/consul-template /usr/local/bin/


groupadd --system consul
useradd -s /sbin/nologin --system -g consul consul
chown consul:consul /usr/local/bin/consul-template
chmod 755 /usr/local/bin/consul-template
mkdir /etc/consul-template
chown consul:consul /etc/consul-template
chmod 775 /etc/consul-template

echo "[3]: create service systemd consul-template"
echo '
[Unit]
Description=Consul Template adon to consul
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=root
Group=consul
ExecStart=/usr/local/bin/consul-template \
  -consul-addr 192.168.57.10:8500 \
  -template "/etc/consul-template/haproxy.cfg.tmpl:/etc/haproxy/haproxy.cfg:service haproxy reload"

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul

[Install]
WantedBy=multi-user.target
' >/etc/systemd/system/consul-template.service

echo "[4]: create service systemd consul-template"
echo '
# template consul template pour haproxy
global
        daemon
        maxconn 256

defaults
        mode http
        timeout connect 5000ms
        timeout client 50000ms
        timeout server 50000ms

frontend http_standard
bind "*:80"
mode http
{{ range services }}{{ if ne .Name "consul" }}
acl front_{{ .Name }} hdr_dom(host) -i {{ range .Tags }}{{ if . | regexMatch "url:(.*)" }}{{ . | regexReplaceAll "url:(.*)" "$1" }}{{ end }}{{ end }}
use_backend back_{{ .Name }} if front_{{ .Name }}
{{ end }}{{ end }}


{{ range services }}{{ if ne .Name "consul" }}
backend back_{{.Name}}{{$service:=.Name }}
  mode http
  option http-server-close
  balance roundrobin{{range service $service }}
  server {{ .Node }} {{.Address }}:{{ .Port }} {{ end }}
{{end}}
{{end}}
'>/etc/consul-template/haproxy.cfg.tmpl

echo "[5]: start consul template"
service consul-template start
systemctl enable consul-template

echo "[6]: add user xavki / psswd= password"
useradd -m -s /bin/bash -p sa3tHJ3/KuYvI -U xavki
echo "%xavki ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/xavki


echo "END - install haproxy"
